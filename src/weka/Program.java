package weka;

import weka.ui.form.MainForm;

import javax.swing.*;

/**
 * Created by tuanna on 10/05/2017.
 */
public class Program {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                MainForm mainForm = new MainForm();
                mainForm.pack();
                mainForm.setLocationRelativeTo(null);
                mainForm.setVisible(true);
            }
        });
    }
}
