package weka.ui.form;

import java.awt.Cursor;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.filechooser.FileNameExtensionFilter;

import weka.api.clustering.BaseClustering;
import weka.api.clustering.DBScanClustering;
import weka.api.clustering.EMClustering;
import weka.api.clustering.SimpleKMClustering;
import weka.api.converter.CSV2ARFFConverter;
import weka.api.lib.ClusteringObserver;
import weka.clusterers.DBSCAN;
import weka.ui.dialog.BaseSettingDialog;
import weka.ui.dialog.DBSCANSettingDialog;
import weka.ui.dialog.EMSettingDialog;
import weka.ui.dialog.SimpleKMeanSettingDialog;

/**
 * Created by tuanna on 10/05/2017.
 */
public class MainForm extends JFrame implements ClusteringObserver {

    private JPanel panelMain;
    private JTextField textFieldInput;
    private JTextField textFieldOutput;
    private JComboBox comboBoxAlgorithm;
    private JButton buttonChooseInput;
    private JButton buttonChooseOutput;
    private JButton buttonOK;
    private JTextPane textPanelConsole;
    private JTextField textFieldInputCSV;
    private JTextField textFieldOutputARFF;
    private JButton buttonInputCSV;
    private JButton buttonOutputARFF;
    private JButton buttonConvert;
    private JTextArea textAreaSettings;
    private JButton buttonAlgorithmSetting;
    private JFileChooser fileChooserInput;
    private JFileChooser fileChooserOutput;
    private JFileChooser fileChooserInputCSV;
    private JFileChooser fileChooserOutputARFF;

    private Map<String, BaseClustering> clusterers;
    private Map<String, BaseSettingDialog> dialogs;
    private CSV2ARFFConverter csv2ARFFConverter = new CSV2ARFFConverter();

    public MainForm() throws HeadlessException {
        setTitle("Weka Utilities");
        setContentPane(panelMain);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        init();
    }

    private void init() {
        buttonChooseInput.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fileChooserInput.showOpenDialog(MainForm.this);
            }
        });
        buttonChooseOutput.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fileChooserOutput.showOpenDialog(MainForm.this);
            }
        });
        buttonOK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                doClustering();
            }
        });
        buttonConvert.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                doConvert();
            }
        });
        buttonInputCSV.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fileChooserInputCSV.showOpenDialog(MainForm.this);
            }
        });
        buttonOutputARFF.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fileChooserOutputARFF.showOpenDialog(MainForm.this);
            }
        });
        buttonAlgorithmSetting.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showSettingDialog();
            }
        });

        String curDir = System.getProperty("user.dir");
        FileNameExtensionFilter filterCSV = new FileNameExtensionFilter("CSV", "csv");
        FileNameExtensionFilter filterTXT = new FileNameExtensionFilter("Text file", "txt");
        FileNameExtensionFilter filterARFF = new FileNameExtensionFilter("ARFF", "ARFF");

        fileChooserInput = new JFileChooser(curDir);
        fileChooserInput.addChoosableFileFilter(filterARFF);
        fileChooserInput.setAcceptAllFileFilterUsed(false);
        fileChooserInput.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                textFieldInput.setText(null != fileChooserInput.getSelectedFile() ?
                        appendExt(fileChooserInput.getSelectedFile().getAbsolutePath(), ".arff") :
                        "");
            }
        });

        fileChooserOutput = new JFileChooser(curDir);
        fileChooserOutput.addChoosableFileFilter(filterTXT);
        fileChooserOutput.setAcceptAllFileFilterUsed(false);
        fileChooserOutput.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                textFieldOutput.setText(null != fileChooserOutput.getSelectedFile() ?
                        appendExt(fileChooserOutput.getSelectedFile().getAbsolutePath(), ".txt") :
                        "");
            }
        });

        fileChooserInputCSV = new JFileChooser(curDir);
        fileChooserInputCSV.addChoosableFileFilter(filterCSV);
        fileChooserInputCSV.setAcceptAllFileFilterUsed(false);
        fileChooserInputCSV.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                textFieldInputCSV.setText(null != fileChooserInputCSV.getSelectedFile() ?
                        appendExt(fileChooserInputCSV.getSelectedFile().getAbsolutePath(), ".csv") :
                        "");
            }
        });

        fileChooserOutputARFF = new JFileChooser(curDir);
        fileChooserOutputARFF.addChoosableFileFilter(filterARFF);
        fileChooserOutputARFF.setAcceptAllFileFilterUsed(false);
        fileChooserOutputARFF.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                textFieldOutputARFF.setText(null != fileChooserOutputARFF.getSelectedFile() ?
                        appendExt(fileChooserOutputARFF.getSelectedFile().getAbsolutePath(),
                                ".arff") :
                        "");
            }
        });

        // Clusterers
        clusterers = new HashMap<>();

        SimpleKMClustering simpleKM = new SimpleKMClustering();
        simpleKM.setObserver(this);
        clusterers.put(Algorithm.SIMPLE_KMEANS.getKey(), simpleKM);

        EMClustering em = new EMClustering();
        em.setObserver(this);
        clusterers.put(Algorithm.EM.getKey(), em);

        DBScanClustering dbscan = new DBScanClustering();
        dbscan.setObserver(this);
        clusterers.put(Algorithm.DBSCAN.getKey(), dbscan);

        // Algorithms
        comboBoxAlgorithm.addItem(Algorithm.EM);
        comboBoxAlgorithm.addItem(Algorithm.SIMPLE_KMEANS);
        comboBoxAlgorithm.addItem(Algorithm.DBSCAN);

        comboBoxAlgorithm.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                    updateSettingInfo();
            }
        });

        // Dialogs
        dialogs = new HashMap<>();
        final BaseSettingDialog simpleKMSettingDlg = new SimpleKMeanSettingDialog(this,
                Algorithm.SIMPLE_KMEANS.toString().concat(" setting"));
        simpleKMSettingDlg.addConfirmActionListeners(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settingDialogOK(simpleKMSettingDlg);
            }
        });

        final BaseSettingDialog dbscanSettingDlg = new DBSCANSettingDialog(this,
                Algorithm.DBSCAN.toString().concat(" setting"));
        dbscanSettingDlg.addConfirmActionListeners(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settingDialogOK(dbscanSettingDlg);
            }
        });

        final BaseSettingDialog emSettingDlg = new EMSettingDialog(this,
                Algorithm.EM.toString().concat(" setting"));
        emSettingDlg.addConfirmActionListeners(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settingDialogOK(emSettingDlg);
            }
        });

        dialogs.put(Algorithm.SIMPLE_KMEANS.getKey(), simpleKMSettingDlg);
        dialogs.put(Algorithm.DBSCAN.getKey(), dbscanSettingDlg);
        dialogs.put(Algorithm.EM.getKey(), emSettingDlg);
    }


    private void updateSettingInfo() {
        BaseSettingDialog dialog = getBaseSettingDialog();
        if (dialog == null) {
            return;
        }

        textAreaSettings.setText(dialog.buildSettingParams().toString());
    }

    private void showSettingDialog() {
        BaseSettingDialog dialog = getBaseSettingDialog();
        if (dialog == null) {
            return;
        }

        dialog.pack();
        dialog.setLocationRelativeTo(this);
        dialog.setVisible(true);
    }

    private void settingDialogOK(BaseSettingDialog dlg) {
        textAreaSettings.setText(dlg.buildSettingParams().toString());
    }

    private String appendExt(String path, String ext) {
        return path.endsWith(ext) ? path : path.concat(ext);
    }

    private void doConvert() {
        try {
            csv2ARFFConverter
                    .convert(new File(textFieldInputCSV.getText()),
                            new File(textFieldOutputARFF.getText()));
            JOptionPane.showMessageDialog(this, "Convert complete.");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this,
                    e.getMessage(), "Convert error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void doClustering() {
        if (null == textFieldInput.getText() || textFieldInput.getText().trim().length() == 0) {
            printToScreen("Input file required!");
            return;
        }

        if (null == textFieldOutput.getText() || textFieldOutput.getText().trim().length() == 0) {
            printToScreen("Output file required!");
            return;
        }

        BaseClustering clusterer = getBaseClustering();
        if (clusterer == null) {
            return;
        }

        BaseSettingDialog dialog = getBaseSettingDialog();
        if (dialog == null) {
            return;
        }

        try {
            clusterer.process(new File(textFieldInput.getText()),
                    new File(textFieldOutput.getText()), dialog.buildSettingParams());
        } catch (Exception e) {
            printToScreen(e);
            onError(e);
        }
    }


    private BaseSettingDialog getBaseSettingDialog() {
        int idx = comboBoxAlgorithm.getSelectedIndex();
        if (idx < 0) {
            return null;
        }

        Algorithm alg = (Algorithm) comboBoxAlgorithm.getItemAt(idx);
        if (null == alg) {
            return null;
        }

        BaseSettingDialog dialog = dialogs.get(alg.getKey());
        if (null == dialog) {
            return null;
        }
        return dialog;
    }

    private BaseClustering getBaseClustering() {
        int idx = comboBoxAlgorithm.getSelectedIndex();
        if (idx < 0) {
            return null;
        }

        Algorithm alg = (Algorithm) comboBoxAlgorithm.getItemAt(idx);
        BaseClustering clusterer = clusterers.get(alg.getKey());
        if (null == clusterer) {
            return null;
        }
        return clusterer;
    }

    @Override
    public void onStart() {
        buttonOK.setEnabled(false);
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    }

    @Override
    public void printToScreen(Object data) {
        textPanelConsole.setText(data.toString());
    }

    @Override
    public void onStop() {
        buttonOK.setEnabled(true);
        setCursor(Cursor.getDefaultCursor());
    }

    @Override
    public void onError(Exception e) {
        buttonOK.setEnabled(true);
        setCursor(Cursor.getDefaultCursor());
    }

    public enum Algorithm {
        EM("em"),
        SIMPLE_KMEANS("simple_kmeans"),
        DBSCAN("dbscan");

        String key;

        Algorithm(String key) {
            this.key = key;
        }

        public String getKey() {
            return key;
        }
    }
}
