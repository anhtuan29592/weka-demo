package weka.ui.dialog;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

public class EMSettingDialog extends BaseSettingDialog {

    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField textFieldNumClusters;
    private JTextField textFieldNumExclusionSlots;
    private JTextField textFieldNumFolds;
    private JTextField textFieldSeed;

    public EMSettingDialog(Frame owner, String title) {
        super(owner, title);
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK(buttonOK);
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
                                               public void actionPerformed(ActionEvent e) {
                                                   onCancel();
                                               }
                                           }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
                JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    @Override
    public Map<String, String> buildSettingParams() {
        Map<String, String> params = new HashMap<>();
        params.put(Constants.NUM_CLUSTER, textFieldNumClusters.getText());
        params.put(Constants.SEED, textFieldSeed.getText());
        params.put(Constants.NUM_EXCLUSION_SLOTS, textFieldNumExclusionSlots.getText());
        params.put(Constants.NUM_FOLDS, textFieldNumFolds.getText());
        return params;
    }
}
