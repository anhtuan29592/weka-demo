package weka.ui.dialog;

import java.awt.Component;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.JDialog;

/**
 * Created by tuanna on 5/24/17.
 */
public abstract class BaseSettingDialog extends JDialog {

    private List<ActionListener> confirmListeners = new ArrayList<>();

    public BaseSettingDialog(Frame owner, String title) {
        super(owner, title);
    }

    public void addConfirmActionListeners(ActionListener listener) {
        confirmListeners.add(listener);
    }


    protected void onOK(Component component) {
        for (ActionListener listener : confirmListeners) {
            listener.actionPerformed(
                    new ActionEvent(component, ActionEvent.ACTION_PERFORMED, null));
        }
        dispose();
    }

    protected void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public abstract Map<String, String> buildSettingParams();


    public interface Constants {

        String NUM_CLUSTER = "numClusters";
        String EPSILON = "epsilon";
        String MIN_PTS = "minPoints";
        String MAX_ITERATIONS = "maxIterations";
        String SEED = "seed";
        String NUM_EXCLUSION_SLOTS = "numExclusionSlots";
        String NUM_FOLDS = "numFolds";
    }
}
