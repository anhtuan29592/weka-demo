package weka.api.converter;

import java.io.File;

import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.core.converters.CSVLoader;

/**
 * Created by tuanna on 5/16/17.
 */
public class CSV2ARFFConverter {

    public void convert(File input, File output) throws Exception {
        // load CSV
        CSVLoader loader = new CSVLoader();
        loader.setSource(input);
        Instances data = loader.getDataSet();//get instances object

        // save ARFF
        ArffSaver saver = new ArffSaver();
        saver.setInstances(data);//set the dataset we want to convert
        //and save as ARFF
        saver.setFile(output);
        saver.writeBatch();
    }
}
