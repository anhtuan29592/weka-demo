package weka.api.lib;

/**
 * Created by tuanna on 5/16/17.
 */
public interface ClusteringObserver {

    void onStart();

    void printToScreen(Object data);

    void onStop();

    void onError(Exception e);
}
