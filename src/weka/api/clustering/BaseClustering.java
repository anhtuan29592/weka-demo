package weka.api.clustering;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import weka.api.lib.ClusteringObserver;
import weka.clusterers.AbstractClusterer;
import weka.clusterers.ClusterEvaluation;
import weka.clusterers.DensityBasedClusterer;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;

/**
 * Created by tuanna on 5/16/17.
 */
public abstract class BaseClustering {

    private ClusteringObserver observer = null;

    public void setObserver(ClusteringObserver observer) {
        this.observer = observer;
    }

    public void process(File input, File output, Map<String, String> options) throws Exception {
        observer.onStart();

        ConverterUtils.DataSource source = new ConverterUtils.DataSource(
                new FileInputStream(input));
        Instances dataSet = source.getDataSet();
        AbstractClusterer clusterer = getClusterer(options);

        clusterer.buildClusterer(dataSet);

        ClusterEvaluation clsEval = new ClusterEvaluation();

        int cnum;
        ConverterUtils.DataSource source1 = new ConverterUtils.DataSource(dataSet);

        clsEval.setClusterer(clusterer);
        clsEval.evaluateClusterer(source1.getDataSet());

        observer.printToScreen(clsEval.clusterResultsToString());

        Instances testRaw = source1.getStructure(dataSet.classIndex());
        Map<Integer, List<Instance>> instanceStats = new HashMap<>();
        while (source1.hasMoreElements(testRaw)) {
            Instance inst = source1.nextElement(testRaw);
            cnum = -1;
            try {
                if (clusterer instanceof DensityBasedClusterer) {
                    cnum = clusterer.clusterInstance(inst);
                } else {
                    cnum = clusterer.clusterInstance(inst);
                }
            } catch (Exception e) {

            }

            if (cnum != -1) {
                if (instanceStats.get(cnum) == null) {
                    instanceStats.put(cnum, new ArrayList<Instance>());
                }
                instanceStats.get(cnum).add(inst);
            }
        }

        BufferedWriter writer = new BufferedWriter(new FileWriter(output));
        try {
            Iterator<Entry<Integer, List<Instance>>> it = instanceStats.entrySet().iterator();
            while (it.hasNext()) {
                Entry<Integer, List<Instance>> next = it.next();
                writer.write("Cluster #" + next.getKey() + ", numOfElement: " + next.getValue().size());
                writer.newLine();

                Iterator<Instance> itVal = next.getValue().iterator();
                while (itVal.hasNext()) {
                    writer.write(itVal.next().toString());
                    writer.newLine();
                }
                writer.newLine();
            }
        } catch (Exception e) {
            observer.onError(e);
        } finally {
            writer.flush();
        }

        observer.onStop();
    }

    protected abstract AbstractClusterer getClusterer(Map<String, String> options) throws Exception;
}
