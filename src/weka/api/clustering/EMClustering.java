package weka.api.clustering;

import java.util.Map;

import weka.clusterers.AbstractClusterer;
import weka.clusterers.DBSCAN;
import weka.clusterers.EM;
import weka.ui.dialog.BaseSettingDialog.Constants;

/**
 * Created by tuanna on 5/19/17.
 */
public class EMClustering extends BaseClustering {

    @Override
    protected AbstractClusterer getClusterer(Map<String, String> options) throws Exception {
        //number of clusters
        EM clusterer = new EM();
        clusterer.setNumClusters(Integer.valueOf(options.get(Constants.NUM_CLUSTER) != null ? options.get(Constants.NUM_CLUSTER) : "-1"));
        clusterer.setNumExecutionSlots(Integer.valueOf(options.get(Constants.NUM_EXCLUSION_SLOTS) != null ? options.get(Constants.NUM_EXCLUSION_SLOTS) : "1"));
        clusterer.setNumFolds(Integer.valueOf(options.get(Constants.NUM_FOLDS) != null ? options.get(Constants.NUM_FOLDS) : "10"));
        clusterer.setSeed(Integer.valueOf(options.get(Constants.SEED) != null ? options.get(Constants.SEED) : "100"));
        return clusterer;
    }
}
