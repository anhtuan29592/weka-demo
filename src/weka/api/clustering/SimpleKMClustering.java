package weka.api.clustering;

import java.util.Map;

import weka.clusterers.AbstractClusterer;
import weka.clusterers.SimpleKMeans;
import weka.ui.dialog.BaseSettingDialog.Constants;

/**
 * Created by tuanna on 5/16/17.
 */
public class SimpleKMClustering extends BaseClustering {

    @Override
    protected AbstractClusterer getClusterer(Map<String, String> options) throws Exception {
        //number of clusters
        SimpleKMeans clusterer = new SimpleKMeans();
        clusterer.setNumClusters(Integer.valueOf(
                options.get(Constants.NUM_CLUSTER) != null ? options.get(Constants.NUM_CLUSTER)
                        : "-1"));
        clusterer.setMaxIterations(Integer.valueOf(
                options.get(Constants.MAX_ITERATIONS) != null ? options
                        .get(Constants.MAX_ITERATIONS) : "500"));
        clusterer.setSeed(Integer.valueOf(
                options.get(Constants.SEED) != null ? options.get(Constants.SEED) : "10"));
        clusterer.setNumExecutionSlots(Integer.valueOf(
                options.get(Constants.NUM_EXCLUSION_SLOTS) != null ? options.get(Constants.NUM_EXCLUSION_SLOTS) : "1"));
        return clusterer;
    }
}
