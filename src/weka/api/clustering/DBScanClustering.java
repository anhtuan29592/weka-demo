package weka.api.clustering;

import java.util.Map;

import weka.clusterers.AbstractClusterer;
import weka.clusterers.DBSCAN;
import weka.ui.dialog.BaseSettingDialog;
import weka.ui.dialog.BaseSettingDialog.Constants;

/**
 * Created by tuanna on 5/19/17.
 */
public class DBScanClustering extends BaseClustering {

    @Override
    protected AbstractClusterer getClusterer(Map<String, String> options) throws Exception {
        //number of clusters
        DBSCAN clusterer = new DBSCAN();
        clusterer.setEpsilon(Double.valueOf(options.get(Constants.EPSILON) != null ? options.get(Constants.EPSILON) : "0.9"));
        clusterer.setMinPoints(Integer.valueOf(options.get(Constants.MIN_PTS) != null ? options.get(Constants.MIN_PTS) : "6"));
        return clusterer;
    }
}
